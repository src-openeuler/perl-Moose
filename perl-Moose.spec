%global _empty_manifest_terminate_build 0
%global perl_vendorlib64 /usr/lib64/perl5/
Name:           perl-Moose
Version:        2.2207
Release:        1
Summary:        The Moose metaclass
License:        GPL-1.0-only or Artistic-1.0-Perl
Group:          Development/Libraries
URL:            https://metacpan.org/dist/Moose
Source0:        https://www.cpan.org/authors/id/E/ET/ETHER/Moose-%{version}.tar.gz
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl => 2.008001
BuildRequires:  perl(strict)
BuildRequires:  perl(warnings)
BuildRequires:  perl(lib)
BuildRequires:  perl(ExtUtils::HasCompiler)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Dist::CheckConflicts)
BuildRequires:  perl(Carp)
BuildRequires:  perl(Moo)
BuildRequires:  perl(Class::Load)
BuildRequires:  perl(Class::Load::XS)
BuildRequires:  perl(Specio)
BuildRequires:  perl(Data::OptList)
BuildRequires:  perl(Devel::GlobalDestruction)
BuildRequires:  perl(Devel::OverloadInfo)
BuildRequires:  perl(Devel::StackTrace)
BuildRequires:  perl(Dist::CheckConflicts)
BuildRequires:  perl(Eval::Closure)
BuildRequires:  perl(List::Util)
BuildRequires:  perl(MRO::Compat)
BuildRequires:  perl(Module::Runtime)
BuildRequires:  perl(Module::Runtime::Conflicts)
BuildRequires:  perl(Package::DeprecationManager)
BuildRequires:  perl(Package::Stash)
BuildRequires:  perl(Package::Stash::XS)
BuildRequires:  perl(Params::Util)
BuildRequires:  perl(Scalar::Util)
BuildRequires:  perl(Sub::Exporter)
BuildRequires:  perl(Sub::Util)
BuildRequires:  perl(Try::Tiny)
BuildRequires:  perl(parent)
BuildRequires:  perl(CPAN::Meta::Check)
BuildRequires:  perl(CPAN::Meta::Requirements)
BuildRequires:  perl(File::Spec)
BuildRequires:  perl(Module::Metadata)
BuildRequires:  perl(Test::Fatal)
BuildRequires:  perl(Test::Output)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Test::Needs)
BuildRequires:  perl(Test::LeakTrace)
BuildRequires:  perl(Algorithm::C3)
BuildRequires:  perl(SUPER)
BuildRequires:  perl(DBM::Deep)
BuildRequires:  perl(Module::Refresh)
BuildRequires:  perl(Declare::Constraints::Simple)
BuildRequires:  perl(Test::Memory::Cycle)
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))

# hidden from PAUSE
Provides:       perl(Moose::Conflicts) = 0
 
# virtual provides for perl-Any-Moose
Provides:       perl(Any-Moose) = %{version}

%description
This class is a subclass of Class::MOP::Class that provides additional Moose-specific functionality.
To really understand this class, you will need to start with the Class::MOP::Class documentation. This class can be understood as a set of additional features on top of the basic feature provided by that parent class.

%prep
%setup -q -n Moose-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 NO_PERLLOCAL=1
make %{?_smp_mflags}

%install
make pure_install DESTDIR=%{buildroot}
find %{buildroot} -type f -name .packlist -delete

%check
make test

%files 
%doc Changes META.yml
%{perl_vendorlib64}/*
%{_bindir}/*
%{_mandir}/man3/*

%changelog
* Mon Mar 4 2024 woody2918 <wudi1@uniontech.com> - 2.2207-1
- Package init 
